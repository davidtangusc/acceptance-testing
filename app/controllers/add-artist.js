import Ember from 'ember';

export default Ember.Controller.extend({
  actions: {
    createArtist(e) {
      e.preventDefault();
      $.ajax({
        type: 'post',
        url: 'http://itp-api.herokuapp.com/api/artists',
        data: {
          name: this.get('name')
        }
      }).then(() => {
        this.set('name', '');
        this.transitionToRoute('artists');
      });
    }
  }
});
